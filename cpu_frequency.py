import argparse, inspect, multiprocessing, os, re, subprocess, time


def get_cpu_frequency(cpu_num):
    return int(subprocess.check_output(f"cpufreq-info -fc {cpu_num}", shell=True).decode("utf-8"))


def set_cpu_frequency(cpu_num, cpu_frequency):
    subprocess.call(f"sudo cpufreq-set -c {cpu_num} -f {cpu_frequency:.0f}", shell=True)


def set_cpu_governor(cpu_num, governor):
    subprocess.call(f"sudo cpufreq-set -c {cpu_num} -g {governor}", shell=True)
