import argparse, inspect, multiprocessing, os, re, subprocess, time

from cpu_frequency import get_cpu_frequency, set_cpu_frequency, set_cpu_governor


def heavy_operation():
    a = 1
    b = 2
    c = 3
    for i in range(70):
        a *= b**c
        b *= c
        c += 1


def repeat_heavy_operation(process_number, number_of_repetitions=10):
    initial_time = time.time()
    for i in range(number_of_repetitions):
        # print(i)
        before = time.time()
        heavy_operation()
        print(f"process {process_number}, iteration {i}:", time.time() - before)
    return time.time() - initial_time


def main(cpu_0_to_3=None, cpu_4_to_7=None):
    try:
        print(os.getpid())

        print(cpu_0_to_3)
        print(cpu_4_to_7)

        if cpu_0_to_3 is None:
            if cpu_4_to_7 is None:
                exit("You have to use at least 1 CPU")
            else:
                cpu_list = "4-7"
        else:
            if cpu_4_to_7 is None:
                cpu_list = "0-3"
            else:
                cpu_list = "0-7"

        subprocess.call(f"taskset -cp {cpu_list} {os.getpid()}", shell=True)

        print("multiprocessing.cpu_count():", multiprocessing.cpu_count())
        print("len(os.sched_getaffinity(0)):", len(os.sched_getaffinity(0)))

        if cpu_0_to_3:
            print("old frequency CPU 0-3:", get_cpu_frequency(0))
            set_cpu_frequency(1, cpu_0_to_3)
            print("new frequency CPU 0-3:", get_cpu_frequency(0))

        if cpu_4_to_7:
            print("old frequency CPU 4-7:", get_cpu_frequency(4))
            set_cpu_frequency(5, cpu_4_to_7)
            print("new frequency CPU 4-7:", get_cpu_frequency(4))

        number_of_cores = sum([cpu_group is not None for cpu_group in [cpu_0_to_3, cpu_4_to_7]]) * 4

        with multiprocessing.Pool() as pool:
            times = pool.map(repeat_heavy_operation, list(range(number_of_cores)))
            for i, time in enumerate(times):
                print("process", i, "finished in", time)

    except KeyboardInterrupt:
        print("Setting CPU governors back to ondemand...")
        set_cpu_governor(0, "ondemand")
        set_cpu_governor(4, "ondemand")
        print("done")


if __name__ == "__main__":
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument("--cpu-0-3", type=int, metavar="frequency_in_khz", help="Uses CPU 0 to 3 with given frequency.")
    argument_parser.add_argument("--cpu-4-7", type=int, metavar="frequency_in_khz", help="Uses CPU 4 to 7 with given frequency.")
    arguments = argument_parser.parse_args()
    main(cpu_0_to_3=arguments.cpu_0_3, cpu_4_to_7=arguments.cpu_4_7)
