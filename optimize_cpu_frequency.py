#!/usr/bin/python3

import argparse
import inspect
import os
import subprocess
import time

from cpu_frequency import set_cpu_frequency
from bwa_mem_experiments import run_bwa_mem

"""
Optimal frequency calculation parameters defined as constants:
- CPU_4_TO_7_AVAILABLE_FREQUENCIES
    - list of which frequencies (in kiloherz) can be configured for the A15 processor.
- CPU_4_TO_7_REFERENCE_ENERGY_PER_SHORT_READ
    - list of the estimated energy consumption (in watthours) per short read for each of the frequencies in CPU_4_TO_7_AVAILABLE_FREQUENCIES.
- CPU_4_TO_7_REFERENCE_TIME_PER_SHORT_READ
    - list of the estimated time usage (in seconds) per short read for each of the frequencies in CPU_4_TO_7_AVAILABLE_FREQUENCIES.
"""
CPU_4_TO_7_AVAILABLE_FREQUENCIES = [1000000.0, 1100000.0, 1200000.0, 1300000.0, 1400000.0, 1500000.0, 1600000.0, 1700000.0, 1800000.0, 1900000.0, 2000000.0]
CPU_4_TO_7_REFERENCE_ENERGY_PER_SHORT_READ = [1.1548736631944436e-06, 1.1551426215277772e-06, 1.1566841666666656e-06, 1.1634346354166658e-06, 1.181876979166666e-06, 1.2055499479166658e-06, 1.2547315694444434e-06, 1.3088313472222227e-06, 1.3209156944444455e-06, 1.3218901805555543e-06, 1.3253363055555565e-06]
CPU_4_TO_7_REFERENCE_TIME_PER_SHORT_READ = [0.00056425, 0.0005394375, 0.000517625, 0.0004981875, 0.0004815625, 0.0004656875, 0.00045180000000000003, 0.00043894999999999996, 0.00043815, 0.0004375, 0.00043675]

def get_cpu_4_to_7_optimal_frequency(minimum_time_estimate, deadline):
    """
    Calculates the optimal frequency for the A15 processor when the goal is to use as little energy as possible and finish before a deadline.

    Arguments:
    - minimum_time_estimate
        - estimated total time usage if run at most performant frequency.
    - deadline
        - deadline (in seconds) before which the alignment must be finished.

    Returns:
    - optimal frequency for the A15 processor (in kiloherz)
    """
    if minimum_time_estimate == deadline:
        # Avoid numerical problems
        minimum_time_estimate -= minimum_time_estimate / 1_000_000_000
    elif minimum_time_estimate > deadline:
        raise Exception("minimum_time_estimate can not be larger than deadline")
    relative_amount_of_work = minimum_time_estimate / min(CPU_4_TO_7_REFERENCE_TIME_PER_SHORT_READ)
    frequency_indices = range(len(CPU_4_TO_7_AVAILABLE_FREQUENCIES))
    frequency_indices_before_deadline = filter(
        lambda i: CPU_4_TO_7_REFERENCE_TIME_PER_SHORT_READ[i] * relative_amount_of_work < deadline,
        frequency_indices,
    )
    optimal_frequency_index = min(
        frequency_indices_before_deadline,
        key=lambda i: CPU_4_TO_7_REFERENCE_ENERGY_PER_SHORT_READ[i],
    )
    return CPU_4_TO_7_AVAILABLE_FREQUENCIES[optimal_frequency_index]


def optimize_cpu_frequency(minimum_time_estimate, deadline):
    optimal_frequency = get_cpu_4_to_7_optimal_frequency(CPU_4_TO_7_AVAILABLE_FREQUENCIES, CPU_4_TO_7_REFERENCE_ENERGY_PER_SHORT_READ, CPU_4_TO_7_REFERENCE_TIME_PER_SHORT_READ, minimum_time_estimate, deadline)
    print("Setting optimal A15 clock frequency:", optimal_frequency)
    set_cpu_frequency(4, optimal_frequency)


def dvfs_manager():
    """
    This function is a draft.
    """
    # Functions to be defined:
    estimate_minimum_time_usage = None
    exchange_minimum_time_usages_in_swaram_network = None


    # Variables to be defined:
    reference_genome_file_path = None
    short_reads_file_path = None
    output_file_path = None

    # Use an algorithm to estimate the minimum total time usage for the current node with the current input.
    minimum_time_estimate = estimate_minimum_time_usage(reference_genome_file_path, short_reads_file_path)

    # Exchange estimated minimum total time usage with SWARAM network and collect minimum total time usage estimates from all other nodes.
    all_minimum_time_estimates = exchange_minimum_time_usages_in_swaram_network(minimum_time_estimate)

    # Define the deadline as the maximum estimated minimum total time usage in the network.
    deadline = max(all_minimum_time_estimates)

    # Optimize CPU frequency.
    optimize_cpu_frequency(minimum_time_estimate, deadline)

    # Run BWA-MEM.
    run_bwa_mem(reference_genome_file_path, short_reads_file_path, output_file_path)


if __name__ == "__main__":
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument("minimum_time_estimate", type=float)
    argument_parser.add_argument("deadline", type=float)
    arguments = argument_parser.parse_args()
    print(get_cpu_4_to_7_optimal_frequency(**vars(arguments)))
