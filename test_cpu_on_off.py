import argparse, inspect, multiprocessing, os, re, subprocess, time

from cpu_frequency import set_cpu_frequency
from cpu_on_off import power_off_cpu_4_to_7, power_on_cpu_4_to_7


def main():
    try:
        while True:
            power_off_cpu_4_to_7()
            set_cpu_frequency(0, 1_500_000)
            print("Off")
            time.sleep(5)
            power_on_cpu_4_to_7()
            set_cpu_frequency(0, 1_500_000)
            print("On")
            time.sleep(5)
    except KeyboardInterrupt:
        pass
    finally:
        print("Turning CPUs on, then exiting...")
        set_cpu_frequency(0, 1_500_000)
        power_on_cpu_4_to_7()

if __name__ == "__main__":
    argument_parser = argparse.ArgumentParser("Test power CPU on/off")
    arguments = argument_parser.parse_args()
    main()
